# Descripción
*[Detallar a un nivel apropiado el requerimiento. Describir el rol de quien utilizará la funcionalidad, la necesidad y el beneficio de cumplir con esta necesidad. Ver ejemplo]*

**Como** un profesional médico del sistema de historia clínica,
**necesito** poder firmar las atenciones que he finalizado dentro del sistema PRAS,
**para** garantizar la legalidad de los documentos.

# Criterios de aceptación
*[Incluir las validaciones que se espera como mínimo para que el requerimiento se de por aceptado. Ver ejemplo]*

* Se deben mostrar únicamente las atenciones finalizadas.
* Debe permitir firmar una por una o en lote.
* Se envía una notificación de confirmación después de ejecutada la transacción.
* .....

*[Se debe adjuntar prototipos de pantallas, ejemplos, flujos de procesos, documentos normativos, etc. en caso de contar con los mismos y siempre que aporten al entendimiento del requerimiento. Ver ejemplo de flujo]*

## Listo para Trabajar (DoR)
*A ser llenado durante la reunión de Planificación (INVEST)*
* [ ] **I**ndependiente – se puede trabajar sin depender de otros requerimientos
* [ ] **N**egociable – puede ser modificada sin perder su objetivo
* [ ] **V**aliosa – debe agregar valor, el beneficio es claro
* [ ] **E**stimable – se pudo ponderar en la planificación
* [ ] **S**imple – tan pequeña para caber en el sprint
* [ ] **T**esteable – se puede probar, los criterios de aceptación son claros

## Flujo (opcional) 
```mermaid
sequenceDiagram

 Médico ->> Atención Médica: Atención Finalizada
 Atención Médica ->> Bandeja de Firma: Crear documento para firma
 Bandeja de Firma ->> MIRTH: Crear CDA
 MIRTH ->> Bandeja de Firma: Devolver CDA
 Médico ->> Bandeja de Firma: Enviar documentos a firmar
 Bandeja de Firma ->> Firmador: Firmar documento CDA
 Firmador ->> Bandeja de Firma: Devolver XML firmado
 Bandeja de Firma ->> Médico: Mostrar documentos
 
```  
    
Ayuda: [Mermaid](https://mermaidjs.github.io/sequenceDiagram.html)