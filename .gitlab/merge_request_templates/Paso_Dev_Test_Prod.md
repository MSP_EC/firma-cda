**PROCEDIMIENTO DE PASO DE ENTORNO _DEV-->TEST_--_TEST-->PROD**

* **_Información General_**

    ```sh
    OTRS:                          [número_otrs]
    ISSUE:			   [número_issue]
    
    ```


* **_Objetivo del Cambio_**

    ```sh
    ... descripción del cambio ...
    
    ```

* **_Issues Relacionados_**

    * SVE/PRY.2017.06.3#31     Integración de codigo de ejemplo
    * [nombre_proyecto]#[número_issue_1]     [Descripción Issue 1]
    * [nombre_proyecto]#[número_issue_2]     [Descripción Issue 2]
    * [nombre_proyecto]#[número_issue_...]   [Descripción Issue 3]
 

* **_Instrucciones de Respaldo_**

    ```sh
    ...Instrucciones de respaldo...
    
    ```

* **_Instrucciones Infraestructura_**

    ```sh
    ...Instrucciones de infraestructura...
    
    ```

* **_Instrucciones Base de Datos_**

    ```sh
    ...Instrucciones de base de datos...
    
    ```

* **_Instrucciones Aplicación_**

    ```sh
    ...Instrucciones de aplicación...
    
    ```

* **_Instrucciones de Recuperación/Rollback_**

    ```sh
    * Regresar a la version anterior de git
    * Ejecutar los scripts de rolback en la Base de Datos
    
    ```

* **_Criterios de Aceptación_ DEV-->TEST**
    - [ ] Cumple con la guía de pasos a DEV/TEST (../wikis/...)
    - [ ] Criterios de Calidad
        - [ ] Requerimientos cuentan con aprobación de PO
        - [ ] Evidencia de pruebas unitarias
        - [ ] Casos de Prueba subidos al GitLab
        - [ ] Evidencia de pruebas de integración (QC en DEV)
    - [ ] Scripts de BD (si aplica)
    - [ ] OK de GIIS
    - [ ] OK de GIPTIC
    - [ ] OK de GIIRSC


---
- [ ] EJECUTADO EN TEST


---

* **_Criterios de Aceptación_ TEST-->PROD**
    - [ ] Pruebas funcionales PO
    - [ ] Pruebas carga y estrés


---
- [ ] EJECUTADO EN PRODUCCIÓN
