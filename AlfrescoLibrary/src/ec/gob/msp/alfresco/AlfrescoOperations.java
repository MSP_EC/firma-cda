package ec.gob.msp.alfresco;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;

public class AlfrescoOperations {

	private static final Logger log = Logger.getLogger(AlfrescoOperations.class.getName());

	private static final String alfrescoUrl = "http://10.64.102.105:8080/alfresco";

	public String list(String libraryName, String user, String password) {

		String responseBody = "";

		try {
			String url = alfrescoUrl + "/api/-default-/public/cmis/versions/1.1/browser/root/" + libraryName
					+ "?cmisselector=children&succinct=true";

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty("Authorization", getAuthorizationHeader(user, password));

			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				String result = processResponse(connection.getInputStream());

				int inicial = result.indexOf("cmis:contentStreamFileName");

				while (inicial > 0) {
					responseBody = responseBody + ","
							+ result.substring(inicial + 29, result.indexOf("\"", inicial + 29));
					inicial = result.indexOf("cmis:contentStreamFileName", inicial + 30);
				}

			} else {
				log.severe("No se encontro el directorio");
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
			responseBody = e.getMessage();
		}
		return responseBody;
	}

	public String createFolder(String libraryName, String newFolderName, String user, String password) {

		String newFolderUUID = findFolderUUID(libraryName + newFolderName, user, password);

		String responseBody = null;

		if (newFolderUUID.length() == 0) {
			try {

				String nameFolder = "";
				String parent = "";
				if (newFolderName.lastIndexOf("/") > 0) {
					nameFolder = newFolderName.substring(newFolderName.lastIndexOf("/") + 1);

					parent = newFolderName.substring(0, newFolderName.lastIndexOf("/"));
				} else {
					nameFolder = newFolderName;

					parent = "";
				}

				String folderUUID = findFolderUUID(libraryName + parent, user, password);

				String url = alfrescoUrl + "/s/api/node/folder/workspace/SpacesStore/" + folderUUID;

				HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
				connection.setRequestProperty("Authorization", getAuthorizationHeader(user, password));
				connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				connection.setDoOutput(true);
				connection.setRequestMethod("POST");

				OutputStream output = connection.getOutputStream();
				output.write(("{\"name\": \"" + nameFolder + "\",\"title\": \"" + nameFolder + "\",\"description\": \""
						+ nameFolder + "\",\"type\": \"cm:folder\"}").getBytes());

				int responseCode = connection.getResponseCode();
				if (responseCode == 200) {
					responseBody = "true";
				} else {
					log.severe("No se pudo crear la carpeta");
					log.severe(connection.getResponseCode() + ": " + connection.getResponseMessage());
					responseBody = "false";
				}
			} catch (Exception e) {
				log.severe(e.getMessage());
				responseBody = e.getMessage();
			}
		} else {
			responseBody = "true";
		}

		return responseBody;
	}

	public String saveFile(String libraryName, String fileName, File file, String user, String password) {

		String responseBody = "";
		try {

			String folderUuid = findFolderUUID(libraryName, user, password);

			String CRLF = "\r\n";

			String boundary = Long.toHexString(System.currentTimeMillis());

			String url = alfrescoUrl + "/s/api/upload";

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty("Authorization", getAuthorizationHeader(user, password));
			connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

			connection.setDoOutput(true);
			connection.setRequestMethod("POST");

			OutputStream output = connection.getOutputStream();
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, Charset.forName("UTF-8").newEncoder()),
					true);

			// Send normal param.
			writer.append("--" + boundary).append(CRLF);
			writer.append("Content-Disposition: form-data; name=\"filename\"").append(CRLF);
			writer.append(CRLF).append(fileName).append(CRLF).flush();

			writer.append("--" + boundary).append(CRLF);
			writer.append("Content-Disposition: form-data; name=\"destination\"").append(CRLF);
			writer.append(CRLF).append("workspace://SpacesStore/" + folderUuid).append(CRLF).flush();

			writer.append("--" + boundary).append(CRLF);
			writer.append("Content-Disposition: form-data; name=\"createdirectory\"").append(CRLF);
			writer.append(CRLF).append("true").append(CRLF).flush();

			writer.append("--" + boundary).append(CRLF);
			writer.append("Content-Disposition: form-data; name=\"overwrite\"").append(CRLF);
			writer.append(CRLF).append("true").append(CRLF).flush();

			// Send binary file.
			writer.append("--" + boundary).append(CRLF);
			writer.append("Content-Disposition: form-data; name=\"filedata\"; filename=\"" + file.getName() + "\"")
					.append(CRLF);
			writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(file.getName())).append(CRLF);
			writer.append("Content-Transfer-Encoding: binary").append(CRLF);
			writer.append(CRLF).flush();

			InputStream is = new FileInputStream(file);

			byte[] buffer = new byte[4096];
			int length;
			while ((length = is.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}

			is.close();

			output.flush(); // Important before continuing with writer!
			writer.append(CRLF).flush(); // CRLF is important! It indicates end
											// of boundary.

			// End of multipart/form-data.
			writer.append("--" + boundary + "--").append(CRLF).flush();

			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				String result = processResponse(connection.getInputStream());

				int inicial = result.indexOf("SpacesStore") + 12;

				responseBody = result.substring(inicial, result.indexOf("\"", inicial));

			} else {
				log.severe("No se pudo crear el archivo");
				log.severe(connection.getResponseCode() + ": " + connection.getResponseMessage());
				responseBody = "0";
				throw new RuntimeException(connection.getResponseCode() + ": " + connection.getResponseMessage());
			}

		} catch (Exception e) {
			log.severe(e.getMessage());
			responseBody = e.getMessage();
		}
		return responseBody;
	}

	public byte[] getFile(String uuid, String user, String password) {
		try {
			String url = alfrescoUrl + "/api/-default-/public/cmis/versions/1.1/atom/content?id=" + uuid;
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty("Authorization", getAuthorizationHeader(user, password));

			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				InputStream response = connection.getInputStream();

				byte[] buffer = new byte[4096];
				int n;

				ByteArrayOutputStream output = new ByteArrayOutputStream();
				while ((n = response.read(buffer)) != -1) {
					output.write(buffer, 0, n);
				}
				output.close();
				return output.toByteArray();
			} else {
				return null;
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
			return null;
		}
	}

	public byte[] getFile(String libraryName, String fileName, String user, String password) {

		try {
			String uuid = findFileUUID(libraryName, fileName, user, password);
			return getFile(uuid, user, password);
		} catch (Exception e) {
			log.severe(e.getMessage());
			return null;
		}

			
	}

	private String findFolderUUID(String libraryName, String username, String password) {

		String responseBody = "";
		try {
			String nameFolder = "";
			if (libraryName.lastIndexOf("/") > 0) {
				nameFolder = libraryName.substring(libraryName.lastIndexOf("/") + 1);
			} else {
				nameFolder = libraryName;
			}

			String url = alfrescoUrl
					+ "/api/-default-/public/cmis/versions/1.1/browser/?cmisselector=query&succinct=true&q="
					+ "SELECT+cmis:path,+cmis:objectId+FROM+cmis:folder+WHERE+cmis:name='" + nameFolder + "'";

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty("Authorization", getAuthorizationHeader(username, password));
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

			connection.setDoOutput(true);
			connection.setRequestMethod("GET");
			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				InputStream response = connection.getInputStream();

				String result = processResponse(response);
				String replace = libraryName.replace("/", "\\/");
				int path = result.indexOf("cmis:path\":\"\\/" + replace + "\"");

				if (path > 0) {
					int inicial = result.indexOf("cmis:objectId", path) + 16;
					responseBody = result.substring(inicial, result.indexOf("\"", inicial + 16));
				} else {
					responseBody = "";
				}

			} else {
				log.info("No se encontr� la carpeta: " + libraryName);
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		return responseBody;
	}

	private String findFileUUID(String libraryName, String fileName, String username, String password) {

		String responseBody = "";

		try {

			String nameFile = "";
			String parentFolder = "";
			if (fileName.lastIndexOf("/") > 0) {
				nameFile = fileName.substring(fileName.lastIndexOf("/") + 1);

				parentFolder = fileName.substring(0, fileName.lastIndexOf("/"));
			} else {
				nameFile = fileName;

				parentFolder = "";
			}

			String url = alfrescoUrl + "/api/-default-/public/cmis/versions/1.1/browser/root/" + libraryName + "/"
					+ parentFolder + "?cmisselector=children&succinct=true";

			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestProperty("Authorization", getAuthorizationHeader(username, password));

			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				String result = processResponse(connection.getInputStream());

				int inicial = result.indexOf(nameFile);

				responseBody = result.substring(result.indexOf("cmis:objectId", inicial) + 16,
						result.indexOf(";", result.indexOf("cmis:objectId", inicial + 17)));

			} else {
				log.severe("No se encontro el archivo: " + fileName + " en el directorio: " + libraryName);
			}
		} catch (Exception e) {
			log.severe(e.getMessage());
		}
		return responseBody;
	}

	private String getAuthorizationHeader(String user, String password) {
		String authString = user + ":" + password;
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		return "Basic " + new String(authEncBytes);
	}

	private String processResponse(InputStream response) {

		try {
			InputStreamReader isr = new InputStreamReader(response);

			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			return sb.toString();

		} catch (Exception e) {
			log.severe(e.getMessage());
			return "";
		}
	}
}
