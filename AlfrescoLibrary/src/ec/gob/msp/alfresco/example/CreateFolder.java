package ec.gob.msp.alfresco.example;

import ec.gob.msp.alfresco.AlfrescoOperations;

public class CreateFolder {

	public static void main(String[] args) {
		AlfrescoOperations alfrescoOperations = new AlfrescoOperations();

		String resp = alfrescoOperations.createFolder("MSP", "carp1", "admin", "Msp2018");
		System.out.println(resp);

	}
}
