package ec.gob.msp.alfresco.example;

import java.io.File;

import ec.gob.msp.alfresco.AlfrescoOperations;

public class AddFile {

	public static void main(String[] args) {
		AlfrescoOperations alfrescoOperations = new AlfrescoOperations();

		File file = new File("/opt/wildfly-8.2.1.Final/standalone/FIRMA/FIRMAEC/RESULTADO/h914u-s40l-o520o-s322b-n101312x.xml");
		
		String resp = alfrescoOperations.saveFile("CDA-XML-FIRMAMSP", "ArchivoPruebaDos.xml", file, "admin", "Msp2018");
		System.out.println(resp);
	}

}
