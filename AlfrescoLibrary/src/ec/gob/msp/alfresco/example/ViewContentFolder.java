package ec.gob.msp.alfresco.example;

import ec.gob.msp.alfresco.AlfrescoOperations;

public class ViewContentFolder {

	public static void main(String[] args) {
		AlfrescoOperations alfrescoOperations = new AlfrescoOperations();
		
		String resp = alfrescoOperations.list("MSP", "admin", "admin");
		System.out.println(resp);
	}

}
