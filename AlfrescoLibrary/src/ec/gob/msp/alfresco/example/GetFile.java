package ec.gob.msp.alfresco.example;

import java.io.FileOutputStream;

import ec.gob.msp.alfresco.AlfrescoOperations;

public class GetFile {

	public static void main(String[] args) throws Exception {
		AlfrescoOperations alfrescoOperations = new AlfrescoOperations();

		//byte[] resp = alfrescoOperations.getFile("CDA-XML-FIRMAMSP", "ArchivoPrueba.xml", "admin", "Msp2018");
		
		byte[] resp = alfrescoOperations.getFile("e7ae6e3a-ba66-4388-8eab-0cd704601cc8;1.0","admin", "Msp2018");

		try (FileOutputStream fos = new FileOutputStream("/home/dmurillo/FIRMA/FIRMAEC/RESULTADO/e7ae6e3a-ba66-4388-8eab-0cd704601cc8Original.xml")) {
			fos.write(resp);
		}

	}

}
