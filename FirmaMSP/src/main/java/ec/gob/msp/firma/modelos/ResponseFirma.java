package ec.gob.msp.firma.modelos;

public class ResponseFirma {
	private String estado;
	private String mensaje;
	private String url;
	private byte[] documento;
		
	public ResponseFirma() {
		super();
	}

	public ResponseFirma(String estado, String mensaje, String url, byte[] documento) {
		super();
		this.estado = estado;
		this.mensaje = mensaje;
		this.url = url;
		this.documento = documento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public byte[] getDocumento() {
		return documento;
	}

	public void setDocumento(byte[] documento) {
		this.documento = documento;
	}

	
	
}
