package ec.gob.msp.firma.servicios;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import ec.gob.msp.firma.modelos.RequestFirma;
import ec.gob.msp.firma.modelos.ResponseFirma;
import ec.gob.msp.firma.utils.FileUtils;
import eu.europa.esig.dss.DSSDocument;
import eu.europa.esig.dss.DigestAlgorithm;
import eu.europa.esig.dss.FileDocument;
import eu.europa.esig.dss.Policy;
import eu.europa.esig.dss.SignatureLevel;
import eu.europa.esig.dss.SignaturePackaging;
import eu.europa.esig.dss.SignatureValue;
import eu.europa.esig.dss.ToBeSigned;
import eu.europa.esig.dss.token.AbstractSignatureTokenConnection;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.xades.XAdESSignatureParameters;
import eu.europa.esig.dss.xades.signature.XAdESService;

public class FirmaXmlXadesBWithSelfSignedCertificate extends Firma {

	public static ResponseFirma firmarXmlXadesBWithSelfSignedCertificate(RequestFirma paramFirma) {
		ResponseFirma response = new ResponseFirma(); 
		try {
			
			if(FileUtils.isXML(paramFirma.getDocumento())){
				DSSDocument toSignDocument = new FileDocument(FileUtils.createTempFile(paramFirma.getDocumento()));
				
				
				AbstractSignatureTokenConnection signingToken = null;
				//signingToken = new Pkcs12SignatureToken(getPathFromResource("/bolivar_david_murillo_uquillas.p12"),paramFirma.getKeyToken());
				
				signingToken = new Pkcs12SignatureToken("/opt/wildfly-8.2.1.Final/standalone/FIRMA/FIRMAEC/FIRMA/bolivar_david_murillo_uquillas.p12","Uni20141");
				
				// Create token connection base on a self sign certificate
				privateKey = signingToken.getKeys().get(0);

				// Preparing parameters for the XAdES signature
				XAdESSignatureParameters parameters = new XAdESSignatureParameters();
				// We choose the level of the signature (-B, -T, -LT, -LTA).
				parameters.setSignatureLevel(SignatureLevel.XAdES_BASELINE_B);
				// We choose the type of the signature packaging (ENVELOPED, ENVELOPING, DETACHED).
				parameters.setSignaturePackaging(SignaturePackaging.ENVELOPED);
				// We set the digest algorithm to use with the signature algorithm. You must use the
				// same parameter when you invoke the method sign on the token. The default value is
				// SHA256
				parameters.setDigestAlgorithm(DigestAlgorithm.SHA256);
				/*
				 * Se aumenta por standar IHE -DSG
				 * 06/04/2018
				 * JB - BM
				 * */
				parameters.setSignedPropertiesCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS);
				parameters.setSignedInfoCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS);
				
				Policy policy = new Policy();
				policy.setDescription("detached");
				policy.setId("urn:ihe:iti:dsg:detached:2014");
				
				
				//parameters.setBLevelParams(bLevelParams);

				// We set the signing certificate
				parameters.setSigningCertificate(privateKey.getCertificate());
				// We set the certificate chain
				parameters.setCertificateChain(privateKey.getCertificateChain());

				// Create common certificate verifier
				CommonCertificateVerifier commonCertificateVerifier = new CommonCertificateVerifier();
				// Create XAdES xadesService for signature
				XAdESService service = new XAdESService(commonCertificateVerifier);

				// Get the SignedInfo XML segment that need to be signed.
				ToBeSigned dataToSign = service.getDataToSign(toSignDocument, parameters);

				// This function obtains the signature value for signed information using the
				// private key and specified algorithm
				DigestAlgorithm digestAlgorithm = parameters.getDigestAlgorithm();
				SignatureValue signatureValue = signingToken.sign(dataToSign, digestAlgorithm, privateKey);

				// We invoke the xadesService to sign the document with the signature value obtained in
				// the previous step.
				DSSDocument signedDocument = service.signDocument(toSignDocument, parameters, signatureValue);
				String repositorio = "/opt/wildfly-8.2.1.Final/standalone/FIRMA/FIRMAEC/RESULTADO/";

				signedDocument.save(repositorio+toSignDocument.getName());
				
				byte [] contenido = null; 
				contenido = new byte[(int) signedDocument.openStream().read()];
				signedDocument.openStream().read(contenido,0,contenido.length);
				
				
				response.setEstado("1");
				response.setMensaje("OK");
				response.setUrl(repositorio+toSignDocument.getName());
				response.setDocumento(contenido);
			}else{
				response.setEstado("-1");
				response.setMensaje("ERROR "+"La informacion enviada no es xml");
				response.setUrl("");
				response.setDocumento(null);
			}
			
		} catch (Exception e) {
			response.setEstado("-1");
			response.setMensaje("ERROR "+e.getMessage());
			response.setUrl("");
			response.setDocumento(null);
		}
		
		
		return response;
	}
	
	public static ResponseFirma validaDocumento(byte[] data, String archivoXSD) {
		
		ResponseFirma response = new ResponseFirma(); 
		try {
			String log = " ";
			
			Source xmlFile = new StreamSource(FileUtils.createTempFile(data)); 
			Source schemaFile = new StreamSource(new File(archivoXSD));
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
			Schema schema = schemaFactory.newSchema(schemaFile);
			Validator validator = schema.newValidator();
			@SuppressWarnings("rawtypes")
			final List exceptions = new LinkedList();
			validator.setErrorHandler(new ErrorHandler(){

				@SuppressWarnings("unchecked")
				public void warning(SAXParseException exception)
						throws SAXException {
					exceptions.add(exception);
					
				}

				@SuppressWarnings("unchecked")
				public void error(SAXParseException exception)
						throws SAXException {
					exceptions.add(exception);
					
				}

				@SuppressWarnings("unchecked")
				public void fatalError(SAXParseException exception)
						throws SAXException {
					exceptions.add(exception);
					
				}
				
			});
			validator.validate(xmlFile);
			
			if (exceptions.size()==0) { 
				log +=  "FILE " + xmlFile.getSystemId() + " IS VALID";
				response.setEstado("1");
				response.setMensaje("OK");
				response.setUrl("");
				response.setDocumento(null);
			} else { 
				log += "FILE " + xmlFile.getSystemId() + " IS INVALID";
				log += "NUMBER OF ERRORS: "+exceptions.size();
				//logger.info("NUMBER OF ERRORS: "+exceptions.size());
				for(int i = 0; i < exceptions.size(); i++) { 
					/*i=i+1; 
					log += "Error # "+i+":";
					log += "<br/>";
					//logger.info("Error # "+i+":");
					i=i-1; 
					log += " - Line: "+((SAXParseException) exceptions.get(i)).getLineNumber();
					log += "<br/>";
					log += " - Column: "+((SAXParseException) exceptions.get(i)).getColumnNumber();
					log += "<br/>";
					log += " - Error message: "+((Throwable) exceptions.get(i)).getLocalizedMessage();
					log += "<br/>";
					log += "------------------------------";
					log += "<br/>";*/
					
					log += " - Line: "+((SAXParseException) exceptions.get(i)).getLineNumber();
					log += " - Column: "+((SAXParseException) exceptions.get(i)).getColumnNumber();
					log += " - Error message: "+((Throwable) exceptions.get(i)).getLocalizedMessage();
					log += "------------------------------";
					
					/*System.out.println(" - Line: "+((SAXParseException) exceptions.get(i)).getLineNumber());
					System.out.println(" - Column: "+((SAXParseException) exceptions.get(i)).getColumnNumber());
					System.out.println(" - Error message: "+((Throwable) exceptions.get(i)).getLocalizedMessage());
					System.out.println("------------------------------");*/
					//break;
				} 
				response.setEstado("-1");
				response.setMensaje(log);
				response.setUrl("");
				response.setDocumento(null);
			} 
			//System.out.println(log);
		} catch (SAXException e) {
			//System.out.println("Error SAXException "+e.getMessage());
			response.setEstado("-1");
			response.setMensaje("Error SAXException "+e.getMessage());
			response.setUrl("");
			response.setDocumento(null);
		} catch (IOException e) {
			//System.out.println("Error IOException "+e.getMessage());
			response.setEstado("-1");
			response.setMensaje("Error IOException "+e.getMessage());
			response.setUrl("");
			response.setDocumento(null);
		}
		return response;
	}
}