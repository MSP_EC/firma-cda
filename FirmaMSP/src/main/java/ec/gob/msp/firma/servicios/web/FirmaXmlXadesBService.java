package ec.gob.msp.firma.servicios.web;


import javax.jws.WebParam;
import javax.jws.WebService;

import ec.gob.msp.firma.modelos.RequestFirma;
import ec.gob.msp.firma.modelos.ResponseFirma;
import ec.gob.msp.firma.servicios.FirmaXmlXadesBWithSelfSignedCertificate;


@WebService
public class FirmaXmlXadesBService {
	
	public ResponseFirma firmarXmlWithXades(@WebParam(name="token") RequestFirma paramFirma) {
		ResponseFirma respuesta;
		String URLXSD = "/opt/wildfly-8.2.1.Final/standalone/FIRMA/FIRMAEC/FIRMA/cda_flat.xsd";//Firma.getPathFromResource("/cda_flat.xsd");
		respuesta = FirmaXmlXadesBWithSelfSignedCertificate.validaDocumento(paramFirma.getDocumento(), URLXSD);
		if(respuesta.getEstado().equals("1")){
			respuesta = new ResponseFirma();
			respuesta = FirmaXmlXadesBWithSelfSignedCertificate.firmarXmlXadesBWithSelfSignedCertificate(paramFirma);
		}else{
			return respuesta;
		}
		return respuesta;
	}
}
