package ec.gob.msp.firma.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

public final class FileUtils {
	private static final Logger logger = Logger.getLogger(FileUtils.class.getName());

	private FileUtils() {
		// No permitimos la instanciacion
	}

	/**
	 * Crea un fichero ZIP en disco apto para manejarse.
	 * 
	 * @param zipFileData
	 *            Los datos del zip.
	 * @return Fichero Zip.
	 * @throws java.util.zip.ZipException
	 *             Cuando los datos no eran realmente un Zip.
	 * @throws IOException
	 *             Cuando ocurre un error al leer los datos o crear el temporal para
	 *             abrir el Zip.
	 */
	public static ZipFile createTempZipFile(final byte[] zipFileData) throws IOException {

		// Creamos un fichero temporal
		final File tempFile = File.createTempFile("afirmazip", null); //$NON-NLS-1$
		final FileOutputStream fos = new FileOutputStream(tempFile);
		fos.write(zipFileData);
		fos.flush();
		fos.close();
		tempFile.deleteOnExit();
		return new ZipFile(tempFile);
	}
	
	/**
	 * Crea un fichero ZIP en disco apto para manejarse.
	 * 
	 * @param zipFileData
	 *            Los datos del zip.
	 * @return Fichero Zip.
	 * @throws java.util.zip.ZipException
	 *             Cuando los datos no eran realmente un Zip.
	 * @throws IOException
	 *             Cuando ocurre un error al leer los datos o crear el temporal para
	 *             abrir el Zip.
	 */
	public static File createTempFile(final byte[] fileData) throws IOException {

		
		// Creamos un fichero temporal
		final File tempFile = File.createTempFile("afirmar", ".xml"); //$NON-NLS-1$
		final FileOutputStream fos = new FileOutputStream(tempFile);
		fos.write(fileData);
		fos.flush();
		fos.close();
		tempFile.deleteOnExit();
		return tempFile;
	}
	
	public static String createTempFileString(final byte[] fileData) throws IOException {

		
		// Creamos un fichero temporal
		final File tempFile = File.createTempFile("afirmar", ".xml"); //$NON-NLS-1$
		final FileOutputStream fos = new FileOutputStream(tempFile);
		fos.write(fileData);
		fos.flush();
		fos.close();
		tempFile.deleteOnExit();
		return tempFile.getPath();
	}

	/**
	 * Comprueba si los datos proporcionados son un XML v&aacute;lido.
	 * 
	 * @param data
	 *            Datos a evaluar.
	 * @return {@code true} cuando los datos son un XML bien formado. {@code false}
	 *         en caso contrario.
	 */
	public static boolean isXML(final byte[] data) {

		final SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(false);
		factory.setNamespaceAware(true);

		try {
			final SAXParser parser = factory.newSAXParser();
			final XMLReader reader = parser.getXMLReader();
			reader.setErrorHandler(new ErrorHandler() {
				@Override
				public void warning(final SAXParseException e) {
					log(e);
				}

				@Override
				public void fatalError(final SAXParseException e) {
					log(e);
				}

				@Override
				public void error(final SAXParseException e) {
					log(e);
				}

				private void log(final Exception e) {
					logger.fine("El documento no es un XML: " + e); //$NON-NLS-1$ //$NON-NLS-2$
				}
			});
			reader.parse(new InputSource(new ByteArrayInputStream(data)));
		} catch (final Exception e) {
			return false;
		}
		return true;
	}
	public static String consultaXml(File data, String nodo, String nombre) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
		String valor = "";
		
		// Carga del documento xml
 		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
 		DocumentBuilder builder = factory.newDocumentBuilder();
 		Document documento = builder.parse(data);
 		
 		// Preparación de xpath
 		XPath xpath = XPathFactory.newInstance().newXPath();
 		
 		// Consultas
 		NodeList nodos = (NodeList) xpath.evaluate(nodo, documento, XPathConstants.NODESET);

		/*for (int i=0;i<nodos.getLength();i++){
			valor = nodos.item(i).getAttributes().getNamedItem(nombre).toString();
			return valor;
		}*/
 		valor = nodos.item(0).getAttributes().getNamedItem(nombre).getNodeValue().toString();
		return valor;
	}
	
}
