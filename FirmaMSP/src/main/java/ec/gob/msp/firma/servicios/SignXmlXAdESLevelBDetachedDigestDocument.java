/**
 * DSS - Digital Signature Services
 * Copyright (C) 2015 European Commission, provided under the CEF programme
 *
 * This file is part of the "DSS - Digital Signature Services" project.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ec.gob.msp.firma.servicios;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import ec.gob.msp.firma.modelos.ResponseFirma;
import ec.gob.msp.firma.utils.FileUtils;
import eu.europa.esig.dss.DSSDocument;
import eu.europa.esig.dss.DigestAlgorithm;
import eu.europa.esig.dss.DigestDocument;
import eu.europa.esig.dss.FileDocument;
import eu.europa.esig.dss.MimeType;
import eu.europa.esig.dss.Policy;
import eu.europa.esig.dss.SignatureLevel;
import eu.europa.esig.dss.SignaturePackaging;
import eu.europa.esig.dss.SignatureValue;
import eu.europa.esig.dss.ToBeSigned;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.xades.DSSReference;
import eu.europa.esig.dss.xades.XAdESSignatureParameters;
import eu.europa.esig.dss.xades.signature.XAdESService;

/**
 * How to sign with XAdES-BASELINE-B
 */
public class SignXmlXAdESLevelBDetachedDigestDocument extends Firma {

	//public static void main(String[] args) throws IOException {
	public static ResponseFirma signXmlXAdESLevelBDetachedDigestDocument(File file,int purposeCode) {
		
		ResponseFirma response = new ResponseFirma(); 
		String idRoot = "";
		String typeId = "";
		try {
			
			idRoot = FileUtils.consultaXml(file, "//id", "root");
			typeId = FileUtils.consultaXml(file, "//typeId", "root");
			
			// GET document to be signed -
			DSSDocument toSignDocument = new FileDocument(file);
				
			//String computedDigest = Utils.toBase64(DSSUtils.digest(DigestAlgorithm.SHA256, toSignDocument));
			String computedDigest = toSignDocument.getDigest(DigestAlgorithm.SHA256);	          

			DigestDocument digestDocument = new DigestDocument();
			digestDocument.setName(idRoot); //tomar del cda en la cabecra 
			digestDocument.addDigest(DigestAlgorithm.SHA256, computedDigest);

			// Create token connection base on a self sign certificate
			//PasswordProtection pPrc = new PasswordProtection(password.toCharArray());
			signingToken = new Pkcs12SignatureToken("/opt/wildfly-8.2.1.Final/standalone/FIRMA/FIRMAEC/FIRMA/bolivar_david_murillo_uquillas.p12","Uni20141");
			privateKey = signingToken.getKeys().get(0);
			
			// Preparing parameters for the XAdES signature
			XAdESSignatureParameters parameters = new XAdESSignatureParameters();
			// We choose the level of the signature (-B, -T, -LT, -LTA).
			parameters.setSignatureLevel(SignatureLevel.XAdES_BASELINE_B);
			// We choose the type of the signature packaging (ENVELOPED, ENVELOPING, DETACHED).
			parameters.setSignaturePackaging(SignaturePackaging.DETACHED);
			// We set the digest algorithm to use with the signature algorithm. You must use the
			// same parameter when you invoke the method sign on the token. The default value is
			// SHA256
			parameters.setDigestAlgorithm(DigestAlgorithm.SHA256);
			/*
			 * Se aumenta por standar IHE -DSG
			 * 06/04/2018
			 * JB - BM
			 * */
			parameters.setSignedPropertiesCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS);
			parameters.setSignedInfoCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS);
			parameters.setSignWithExpiredCertificate(true);

			
			List<DSSReference> refencias = new ArrayList<>();
			refencias.add(createReference(digestDocument,parameters,typeId) ); //TODO Sacar del documento
			parameters.setReferences(refencias);
			
			Policy policy = new Policy();
			policy.setId("urn:ihe:iti:dsg:detached:2014");
			parameters.bLevel().setSignaturePolicy(policy);
			
			List<String> commitmentTypeIndications = new ArrayList<>();
			PurposeService purposeService = new PurposeService(); 
			commitmentTypeIndications.add(purposeService.getPurpose(purposeCode).getCode());
			parameters.bLevel().setCommitmentTypeIndications(commitmentTypeIndications);
			parameters.isManifestSignature();
			
			
			//belevel sacar
			
			
			// We set the signing certificate
			parameters.setSigningCertificate(privateKey.getCertificate());
			// We set the certificate chain
			parameters.setCertificateChain(privateKey.getCertificateChain());
			
			

			// Create common certificate verifier
			CommonCertificateVerifier commonCertificateVerifier = new CommonCertificateVerifier();
			// Create XAdES xadesService for signature
			XAdESService service = new XAdESService(commonCertificateVerifier);

			// Get the SignedInfo XML segment that need to be signed.
			ToBeSigned dataToSign = service.getDataToSign(digestDocument, parameters);

			// This function obtains the signature value for signed information using the
			// private key and specified algorithm
			DigestAlgorithm digestAlgorithm = parameters.getDigestAlgorithm();
			SignatureValue signatureValue = signingToken.sign(dataToSign, digestAlgorithm, privateKey);	

			// We invoke the xadesService to sign the document with the signature value obtained in
			// the previous step.
			DSSDocument signedDocument = service.signDocument(digestDocument, parameters, signatureValue);
			
			String repositorio = "/opt/wildfly-8.2.1.Final/standalone/FIRMA/FIRMAEC/RESULTADO/";
			signedDocument.save(repositorio+digestDocument.getName()+".xml");
			
			byte[] docFirmado;
			
			try {
				FileInputStream fileStream = new FileInputStream( file = new File(repositorio+digestDocument.getName()+".xml"));
				// Instantiate array
				docFirmado = new byte[(int) file.length()];
			    /// read All bytes of File stream
			    fileStream.read(docFirmado,0,docFirmado.length);
			    fileStream.close();
			} catch (IOException e) {
				docFirmado = null;
			}

			response.setEstado("1");
			response.setMensaje("OK");
			response.setUrl(repositorio+signedDocument.getName()+".xml");
			response.setDocumento(docFirmado);
			
		} catch (Exception e) {
			response.setEstado("-1");
			response.setMensaje("ERROR "+e.getMessage());
			response.setUrl("");
			response.setDocumento(null);
		}
		
		return response;
	}
	
	public static DSSReference createReference(DSSDocument document, XAdESSignatureParameters params, String OIDType) {
		final DSSReference reference = new DSSReference();
		reference.setId(document.getName());
		reference.setType("urn:uuid:" + OIDType);
		final String fileURI = document.getName() != null ? "urn:uuid:" + document.getName() : ""; //TODO comprobar que el campo no esta vacio
		try {
			reference.setUri(URLEncoder.encode(fileURI, "UTF-8"));
		} catch (Exception e) {
			reference.setUri(fileURI);
		}
		reference.setContents(document);
		reference.setDigestMethodAlgorithm(params.getDigestAlgorithm());
		
		MimeType mimeType = new MimeType();
		mimeType.setMimeTypeString("text/xml");
		reference.getContents().setMimeType(mimeType );
		return reference;
	}
	
	public static ResponseFirma validaDocumento(File data, String archivoXSD) {
		
		ResponseFirma response = new ResponseFirma(); 
		try {
			String log = " ";
			
			Source xmlFile = new StreamSource(data); 
			Source schemaFile = new StreamSource(new File(archivoXSD));
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
			Schema schema = schemaFactory.newSchema(schemaFile);
			Validator validator = schema.newValidator();
			@SuppressWarnings("rawtypes")
			final List exceptions = new LinkedList();
			validator.setErrorHandler(new ErrorHandler(){

				@SuppressWarnings("unchecked")
				public void warning(SAXParseException exception)
						throws SAXException {
					exceptions.add(exception);
					
				}

				@SuppressWarnings("unchecked")
				public void error(SAXParseException exception)
						throws SAXException {
					exceptions.add(exception);
					
				}

				@SuppressWarnings("unchecked")
				public void fatalError(SAXParseException exception)
						throws SAXException {
					exceptions.add(exception);
					
				}
				
			});
			validator.validate(xmlFile);
			
			if (exceptions.size()==0) { 
				log +=  "FILE " + xmlFile.getSystemId() + " IS VALID";
				response.setEstado("1");
				response.setMensaje("OK");
				response.setUrl("");
				response.setDocumento(null);
			} else { 
				log += "FILE " + xmlFile.getSystemId() + " IS INVALID";
				log += "NUMBER OF ERRORS: "+exceptions.size();
				for(int i = 0; i < exceptions.size(); i++) { 
					
					log += " - Line: "+((SAXParseException) exceptions.get(i)).getLineNumber();
					log += " - Column: "+((SAXParseException) exceptions.get(i)).getColumnNumber();
					log += " - Error message: "+((Throwable) exceptions.get(i)).getLocalizedMessage();
					log += "------------------------------";
				} 
				response.setEstado("-1");
				response.setMensaje(log);
				response.setUrl("");
				response.setDocumento(null);
			} 
		} catch (SAXException e) {
			response.setEstado("-1");
			response.setMensaje("Error SAXException "+e.getMessage());
			response.setUrl("");
			response.setDocumento(null);
		} catch (IOException e) {
			response.setEstado("-1");
			response.setMensaje("Error IOException "+e.getMessage());
			response.setUrl("");
			response.setDocumento(null);
		}
		return response;
	}
}
