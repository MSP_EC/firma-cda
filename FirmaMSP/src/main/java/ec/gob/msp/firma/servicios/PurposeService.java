package ec.gob.msp.firma.servicios;

import java.util.ArrayList;
import java.util.List;

import ec.gob.msp.firma.modelos.Purpose;

public class PurposeService {
	private List<Purpose> purposes;

	public PurposeService() {
		this.loadPurposes();
	}

	public void loadPurposes() {
		purposes = new ArrayList<>();
		purposes.add(new Purpose(1, "1.2.840.10065.1.12.1.1", "Author’s"));
		purposes.add(new Purpose(2, "1.2.840.10065.1.12.1.2", "Co-Author’s"));
		purposes.add(new Purpose(3, "1.2.840.10065.1.12.1.3", "Co-participant’s"));
		purposes.add(new Purpose(4, "1.2.840.10065.1.12.1.4", "Transcriptionist/Recorder"));
		purposes.add(new Purpose(5, "1.2.840.10065.1.12.1.5", "Verification"));
		purposes.add(new Purpose(6, "1.2.840.10065.1.12.1.6", "Validation"));
		purposes.add(new Purpose(7, "1.2.840.10065.1.12.1.7", "Consent"));
		purposes.add(new Purpose(8, "1.2.840.10065.1.12.1.8", "Signature Witness"));
		purposes.add(new Purpose(9, "1.2.840.10065.1.12.1.9", "Event Witness"));
		purposes.add(new Purpose(10, "1.2.840.10065.1.12.1.10", "Identity Witness"));
		purposes.add(new Purpose(11, "1.2.840.10065.1.12.1.11", "Consent Witness"));
		purposes.add(new Purpose(12, "1.2.840.10065.1.12.1.12", "Interpreter"));
		purposes.add(new Purpose(13, "1.2.840.10065.1.12.1.13", "Review"));
		purposes.add(new Purpose(14, "1.2.840.10065.1.12.1.14", "Source"));
		purposes.add(new Purpose(15, "1.2.840.10065.1.12.1.15", "Addendum"));
		purposes.add(new Purpose(16, "1.2.840.10065.1.12.1.16", "Modification"));
		purposes.add(new Purpose(17, "1.2.840.10065.1.12.1.17", "Administrative(Error/Edit)"));
		purposes.add(new Purpose(18, "1.2.840.10065.1.12.1.18", "Timestamp"));
	}
	
	public Purpose getPurpose(int id) {
		if(id != 0){
			for (Purpose purposeItem : purposes) {
				if(purposeItem.getId() == id){
					return purposeItem;
				}
			}
		}else{
			return purposes.get(0);
		}
		return null;
	}

	public List<Purpose> getPurposes() {
		return purposes;
	}

	public void setPurposes(List<Purpose> purposes) {
		this.purposes = purposes;
	}
	
}
