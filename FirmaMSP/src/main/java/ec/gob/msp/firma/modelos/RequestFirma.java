package ec.gob.msp.firma.modelos;


public class RequestFirma {
	private byte[] documento; 
	private String idSigner;
	private int purposeCode;
	
	public RequestFirma() {
		super();
	}

	public RequestFirma(byte[] documento, String idSigner, int purposeCode) {
		super();
		this.documento = documento;
		this.idSigner = idSigner;
		this.purposeCode = purposeCode;
	}

	public byte[] getDocumento() {
		return documento;
	}

	public void setDocumento(byte[] documento) {
		this.documento = documento;
	}

	public String getIdSigner() {
		return idSigner;
	}

	public void setIdSigner(String idSigner) {
		this.idSigner = idSigner;
	}

	public int getPurposeCode() {
		return purposeCode;
	}

	public void setPurposeCode(int purposeCode) {
		this.purposeCode = purposeCode;
	}

	
}
