package ec.gob.msp.firma.modelos;

public class Purpose {
	private int id;
	private String code;
	private String term;
	
	public Purpose() {
		super();
	}

	public Purpose(int id, String code, String term) {
		super();
		this.id = id;
		this.code = code;
		this.term = term;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}
	

	
	
}
