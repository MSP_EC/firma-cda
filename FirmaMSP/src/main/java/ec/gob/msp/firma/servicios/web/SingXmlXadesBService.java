package ec.gob.msp.firma.servicios.web;

import java.io.File;
import java.io.IOException;

import javax.jws.WebParam;
import javax.jws.WebService;


import ec.gob.msp.firma.modelos.RequestFirma;
import ec.gob.msp.firma.modelos.ResponseFirma;
import ec.gob.msp.firma.servicios.SignXmlXAdESLevelBDetachedDigestDocument;
import ec.gob.msp.firma.utils.FileUtils;


@WebService
public class SingXmlXadesBService {
	public ResponseFirma signXmlXAdESLevelBDetachedDigestDocument(@WebParam(name="documento") byte[] documento, @WebParam(name="idSigner") String idSigner, @WebParam(name="porpuseCode") int porpuseCode) {
		RequestFirma paramFirma = new RequestFirma();
		paramFirma.setDocumento(documento);
		paramFirma.setIdSigner(idSigner);
		paramFirma.setPurposeCode(porpuseCode);
		
		ResponseFirma respuesta = new ResponseFirma();
		String URLXSD = "/opt/wildfly-8.2.1.Final/standalone/FIRMA/FIRMAEC/FIRMA/cda_flat.xsd";
		File data = null;
		try {
			// Carga del documento xml
			data = FileUtils.createTempFile(paramFirma.getDocumento());		
	 		if(FileUtils.isXML(paramFirma.getDocumento())){
	 			respuesta = SignXmlXAdESLevelBDetachedDigestDocument.validaDocumento(data, URLXSD);
	 			if(respuesta.getEstado().equals("1")){
	 				respuesta = new ResponseFirma();
	 				respuesta = SignXmlXAdESLevelBDetachedDigestDocument.signXmlXAdESLevelBDetachedDigestDocument(data, paramFirma.getPurposeCode());
	 			}
	 		}else{
	 			respuesta.setEstado("-1");
	 			respuesta.setMensaje("ERROR "+"La informacion enviada no es xml");
	 			respuesta.setUrl("");
	 			respuesta.setDocumento(null);
			}
			
		} catch (IOException e) {
			respuesta.setEstado("-1");
 			respuesta.setMensaje("ERROR "+"Existe un error en el proceso "+e.getMessage());
 			respuesta.setUrl("");
 			respuesta.setDocumento(null);
		}
		return respuesta;
	}
}
